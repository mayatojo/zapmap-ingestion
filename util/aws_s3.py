import boto3
from botocore.exceptions import ClientError
import os

bucket ='ev-modeling-platform'


def connect_s3():
    """ connects to aws as indentified in credentials.json
        return: session.resource('s3')
    """
    # file_json = cwd + os.sep + 'credentials.json'
    # credentials = open_json(file_json)
    session = boto3.Session()
    s3 = session.resource('s3')
    # s3 = boto3.client('s3')
    return s3


def get_files_in_folder(s3_resource, folder='data',
                        bucket=bucket):
    bucket = s3_resource.Bucket(bucket)

    keys = []
    for obj in bucket.objects.all():
        if obj.key.startswith(folder):
            keys.append(obj.key)

    return keys


def upload_file(s3_resource, s3_location, body, bucket=bucket):
    """ uploads local file to s3
    """
    s3_resource.Object(bucket, s3_location).put(Body=body)
    

def delete_file(s3_resource, s3_location, bucket=bucket):
    """ deletes file from s3
    """
    s3_resource.Object(bucket, s3_location).delete()
    
    
#def upload_file(s3_resource, s3_location, local_location, bucket=bucket):
 #   """ uploads local file to s3
  #  """
   # s3_resource.Object(bucket, s3_location).put(Body=open(local_location, 'rb'))


def download_file(s3_resource, s3_location, local_location, bucket='mayato-geo-pilot'):
    try:
        s3_resource.Bucket(bucket).download_file(s3_location, local_location)
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise

