def date2name(date, format='YYYY_WXX'):
    """date: datetime.date"""
    if format=='YYYY_WXX':
        year = str(date.isocalendar()[0])
        week = str(date.isocalendar()[1]).zfill(2)
        name = year + '_' + 'W' + week
    elif format=='YYYY_MXX':
        year = str(date.isocalendar()[0])
        month = str(date.isocalendar()[2]).zfill(2)
        name = year + '_' + 'M' + month
    else:
        print('unknown format, return date as string')
        return str(date)
    return name

