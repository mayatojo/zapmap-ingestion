import requests
import pandas as pd
from datetime import datetime

from util.general import date2name
from util.aws_s3 import upload_file, connect_s3


# ----------------------------------------------------------------------- SETUP
API_KEY = "4023a6cbe0504dc3a3b5fde5488954e3"
USER = "admin"
PASSWORD = "redsky"

s3_resource = connect_s3()

def lambda_handler(event, context):
    base_url = "https://www.zap-map.com/api/instavoltrapid/" \
               "get-chargers.php?apikey={api_key}"
    url = base_url.format(api_key=API_KEY)
    response = requests.get(url, auth=(USER, PASSWORD))
    df = pd.DataFrame(response.json())

    date = datetime.now().date()
    folder_name = date2name(date, format='YYYY_WXX')
    file_name = 'Zapmap_raw.csv'

    # upload to S3
    key = '1_RawData/Charging_ZapMap/' + folder_name + '/' + file_name
    bucket ='ev-modeling-platform'
    uploadByteStream = bytes(df.to_csv().encode('UTF-8'))
    upload_file(s3_resource, s3_location=key, body=uploadByteStream,
                bucket=bucket)
